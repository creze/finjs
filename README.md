MathFinJS
=========

A small library providing utility methods to financial math for use on creze.com
## Installation

  npm install mathfinjs --save

## Usage

```javascript
  var mathfinjs = require('mathfinjs')
      getCAT = mathfinjs.getCAT;
      getPPMT = mathfinjs.getPPMT;

  var CAT = getCAT(100000, 2000, 52000, 2, mathfinjs.PERIODS.FORTNIGHTLY).formatted;
  var PPMT = getPPMT(0.005, 1, 6, -100000, 0,0).formatted;

  console.log(CAT, PPMT);
```

## Tests

  npm test

## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style.
Add unit tests for any new or changed functionality.

## Release History

* 1.0.0 Initial release