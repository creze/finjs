var gulp = require('gulp');
var mocha = require('gulp-mocha');
var uglify = require('gulp-uglifyjs');
var rename = require("gulp-rename");

/*
Error: Cannot find module 'jsdoc/util/doop'
//var jsdoc = require('gulp-jsdoc');
gulp.task('docs', function() {
	return gulp.src('*.js')
	.pipe(jsdoc('./docs'));
});
*/
gulp.task('test', function() {
	return gulp.src('test/index.js', {read: false})
	.pipe(mocha({reporter: 'nyan'}));
});
gulp.task('default', function() {
	console.log('Available:', ['docs', 'test']);
})
gulp.task('uglify', function() {
  gulp.src('index.js')
    .pipe(uglify())
		.pipe(rename("finjs.min.js"))
    .pipe(gulp.dest('dist/'))
});
