window.mathfinjs =  {
	/** periods constants in EN and ES */
	PERIODS_BY_MONTH: {
		DAILY: 20,
		WEEKLY: 4,
		FORTNIGHTLY: 2,
		MONTHLY: 1,
		QUARTERLY: 0.25,
		SEMESTERLY: 0.5
	},
	PERIODS: {
		DAILY: 365,
		WEEKLY: 52,
		FORTNIGHTLY: 24,
		MONTHLY: 12,
		QUARTERLY: 4,
		SEMESTERLY: 2,
		YEARLY: 1,
	},
	/**
	 * Get the sum of periods using the received param CAT.
	 *
	 * @param {number} total - The total expected
	 * @param {number} charge - The charges that don't be in the lend
	 * @param {number} pay - The pay by period
	 * @param {number} payments - The number of payments received
	 * @param {number} periods - The total periods by year
	 * @param {number} cat - The cat received to do the algorithm
	 * @return {Object}
	 */
	getCATValue: function(total, charge, pay, payments, periods, cat) {
    	var i = 0;
    	var value = charge / Math.pow((1 + (cat / 100)), 0 / periods);

    	for(i = 1; i <= payments; i++){
    		value += pay / Math.pow( (1 + (cat / 100)), i / periods);
    	}
    	return total - value;
    },

    /**
	 * Get CAT of the passed params.
	 *
	 * @param {number} amount - Total amount of the lend without interest
	 * @param {number} charge - The charges the lender do that's not part of the lend
	 * @param {number} byPeriod - The payment by period that the user do
	 * @param {number} totalPayments - Total payments of the lend
	 * @param {number} periodsByYear - Periods by year according to Banxico(Days: 356, Weeks: 52, Fortnight: 24, Months: 12...)
	 * @return {Object}
	 */
	getCAT: function(amount, charge, byPeriod, totalPayments, periodsByYear){
		var cat = 50, catMin = 0, catMax = 0; tempCat = null;

		var counter = 0; tempVal = null;

		do{
			tempVal = mathfinjs.getCATValue(amount, charge, byPeriod, totalPayments, periodsByYear, cat);
			if(tempVal > 0){
				tempCat = cat;
				cat = (cat+ catMin) / 2;
				catMax = tempCat;
			}else if(catMax == 0){
				catMin = cat;
				cat = cat + 50;
			}else{
				tempCat = cat;
				cat = (cat + catMax) / 2;
				catMin = tempCat;
			}
			counter ++;
		} while(counter <= 25);

		return {value: cat, formatted: cat.toFixed(2)};
	},

	/**
	* Get pay for schedule amortization
	*
	* @param {int} r - (interest rate % 12)
	* @param {int} per - period of pay
	* @param {int} nper - term
	* @param {int}	pv - amount * -1
	* @param {int} fv2 - Default 0
	* @param {int} type - Default 0
	* return {Object}
	*/
	getPMT : function (r,nper,pv, fv2, type){
	  var pmt = r / (Math.pow(1 + r, nper) - 1) * -(pv * Math.pow(1 + r, nper) + fv2);
	  return pmt;
	},

	getFV : function (r,  nper,  c, pv, type) {
      var fv2 = -(c * (Math.pow(1 + r, nper) - 1) / r + pv * Math.pow(1 + r, nper));
      return fv2;
	},

	getIPMT : function (r,per, nper, pv, fv2, type){
	   var ipmt2 = mathfinjs.getFV(r, per - 1, mathfinjs.getPMT(r, nper, pv, fv2, type), pv, type) * r;
	  if (type == 1) ipmt2 /= (1 + r);
	  return ipmt2;
	},

	getPPMT : function (r, per, nper, pv, fv2, type){
	   var ppmt = mathfinjs.getPMT(r, nper, pv, fv2, type) - mathfinjs.getIPMT(r, per, nper, pv, fv2, type);
	   return {value:ppmt, formatted: ppmt.toFixed(2)};
	}
}
