var should = require('chai').should(),
	finjs = require('../index'),
	getCAT = finjs.getCAT;
	getPPMT = finjs.getPPMT;
	PERIODS = finjs.PERIODS;


describe('#getCAT', function(){
	it('should return CAT of 159.59% when the lend equals 100k => 1 month @ $52,000 by 2 payments fortnightly', function() {
		should.equal('159.59', getCAT(100000, 2000, 52000, 2, PERIODS.FORTNIGHTLY).formatted);
	});
	it('should return CAT of 246.75% when the lend equals 100k => 1 month @ $26,000 by 4 payments weekly', function() {
		should.equal('246.75', getCAT(100000, 2000, 26000, 4, PERIODS.WEEKLY).formatted);
	});
});
//obtienes la cantidad que se paga a capital
describe('#getPAY', function(){
	it('should return the amount of the payment to capital', function() {
		should.equal('16459.55', getPPMT(0.005, 1, 6, -100000, 0,0).formatted);
	});
});